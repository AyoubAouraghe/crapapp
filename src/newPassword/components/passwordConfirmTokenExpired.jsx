import React from "react";
import Banner from "../../shared/assets/crapappbanner.png";
import { routes } from "../../config";

const PasswordConfirmTokenExpired = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col s8 offset-s2">
                    <div className="card">
                        <div className="card-image">
                            <img src={Banner} className="banner" alt="logo crap app" />
                        </div>
                        <div className="card-content">
                            <div className="row">
                                <p>Your token has expired, please request a new password.</p>
                            </div>
                            <div className="card-action">
                                <a className="waves-effect waves-light btn" href={routes.startScreen}>Request a new password</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PasswordConfirmTokenExpired;