import React, { Component } from 'react';
import Banner from "../assets/crapappbanner.png";

class ErrorPage extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col s8 offset-s2">
                        <div className="card">
                            <div className="card-image">
                                <img src={Banner} className="banner" alt="logo crap app" />
                            </div>
                            <div className="card-content">
                                <div className="row">
                                <p>Something whent wrong. 
                                    We try our best to fix this problem as soon as possible.
                                     Please try again later. 
                                     Sorry for the inconvenience</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ErrorPage;