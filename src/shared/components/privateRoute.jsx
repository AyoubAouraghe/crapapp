import React from 'react';
import { routes, authenticationToken } from '../../config.js';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(props) => (
            window.sessionStorage.getItem(authenticationToken)
              ? <Component {...props} />
              : <Redirect to={routes.login} />
          )} />
    );
}

export default PrivateRoute;